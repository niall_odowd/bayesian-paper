\section{On derivatives}
\label{section3}
\subsection{Useful results on derivatives of Lie-bracket and higher-order product rule}
%######################################################################
%######################################################################
\paragraph{Proposition 4:} For any $\hat{\boldsymbol{a}},\hat{\boldsymbol{b}}\in so(3)$ with corresponding axial vectors $\boldsymbol{a},\boldsymbol{b}\in \mathbb{R}^3$ respectively, the following formula for derivative holds:
%**************************************
%EQUATION:  
%**************************************
\begin{equation}\label{lie_brackets_der}
	\partial^{n}_{\xi}\left[\hat{\boldsymbol{a}},\hat{\boldsymbol{b}}\right]=\sum_{i=0}^n C_i^n\left[\partial_{\xi}^{(n-i)}\hat{\boldsymbol{a}},\partial_{\xi}^{(i)}\hat{\boldsymbol{b}}\right]=\sum_{i=0}^n C_i^n\left[\partial_{\xi}^{(i)}\hat{\boldsymbol{a}},\partial_{\xi}^{(n-i)}\hat{\boldsymbol{b}}\right]
\end{equation}
%**************************************  
\paragraph{Proof:} Using the definition of Lie-bracket in Eq. \eqref{lie_brackets1}, we have
\begin{equation}\label{lie_brackets3}\partial_{\xi}\left[\hat{\boldsymbol{a}},\hat{\boldsymbol{b}}\right]=(\partial_{\xi}\hat{\boldsymbol{a}}.\hat{\boldsymbol{b}}+\hat{\boldsymbol{a}}.\partial_{\xi}\hat{\boldsymbol{b}})-(\partial_{\xi}\hat{\boldsymbol{b}}.\hat{\boldsymbol{a}}+\hat{\boldsymbol{b}}.\partial_{\xi}\hat{\boldsymbol{a}})=\left[\partial_{\xi}\hat{\boldsymbol{a}},\hat{\boldsymbol{b}}\right]+\left[\hat{\boldsymbol{a}},\partial_{\xi}\hat{\boldsymbol{b}}\right].
\end{equation} 

Higher-order derivatives of the Lie-bracket derived using the above result yields an expression given by Eq. \eqref{lie_brackets_der}.  The first and second equality in \eqref{lie_brackets_der} hold since $C_i^n=C^n_{(n-i)}$. This completes the proof. $\square$
%######################################################################
%######################################################################

The result above can be extended to obtain general product rule; from Eq. \eqref{lie_brackets3}, we see that Lie-brackets follow the chain rule just like the product of two scalars or a dot product except for the fact that Lie-brackets are non-commutative. For instance, for a scalar functions $f(\xi)$ and a tensor $\boldsymbol{A}(\xi)$, we have

%**************************************
%EQUATION:  
%**************************************
\begin{equation}\label{general_der}
	\partial^{n}_{\xi}(f\boldsymbol{A})=\sum_{i=0}^n C_i^n\partial_{\xi}^{(n-i)}f.\partial_{\xi}^{(i)}\boldsymbol{A}=\sum_{i=0}^n C_i^n\partial_{\xi}^{(i)}f.\partial_{\xi}^{(n-i)}\boldsymbol{A}.
\end{equation}
%************************************** 
%######################################################################
%######################################################################
\paragraph{Proposition 5:} For any $\hat{\boldsymbol{a}}(\xi)\in so(3)$, the following holds: %**************************************
%EQUATION:  
%**************************************
\begin{equation}\label{lie_brackets_der3}
	\partial^{m}_{\xi}\left[\hat{\boldsymbol{a}},\partial_{\xi}\hat{\boldsymbol{a}}\right]=\sum_{j=0}^{j_{m}^{\text{max}}} b_{mj}\left[\partial_{\xi}^{(j)}\hat{\boldsymbol a},\partial_{\xi}^{(m-j+1)}\hat{\boldsymbol a}\right].
\end{equation}
%************************************** 
where $m,j,j^{\text{max}}_{m},b_{mj}\in\mathbb{Z}^+$, such that the coefficients $j^{\text{max}}_{m}$, and $b_{mj}$ are given as

%**************************************
%EQUATION:  
%**************************************
\begin{equation}\label{lie_brackets_der4}
j^{\text{max}}_{m}=\begin{cases}\text{floor}\left(\frac{m+1}{2}\right), & \text{ if } \frac{m+1}{2}\notin \mathbb{Z}^+\\
\frac{m+1}{2}-1,& \text{ if } \frac{m+1}{2}\in \mathbb{Z}^+
\end{cases};\text{ and }b_{mj}=C_{j}^m-C_{(m-j+1)}^m=C^m_j\left(\frac{m-2j+1}{m-j+1}\right).
\end{equation}
%************************************** 
\paragraph{Proof:}  Using the result \eqref{lie_brackets_der} of \textit{Proposition 4}, we get,
%**************************************
%EQUATION:  
%**************************************
\begin{equation}\label{lie_brackets_der5}
\partial_{\xi}^{m}[\hat{\boldsymbol{a}},\partial_{\xi}\hat{\boldsymbol{a}}]=\sum_{j=0}^{m}C_{j}^m\left[\partial_{\xi}^{(j)}\hat{\boldsymbol{a}},\partial_{\xi}^{(m-j+1)}\hat{\boldsymbol{a}}\right]=\overbrace{C^m_0\left[\hat{\boldsymbol{a}},\partial_{\xi}^{(m+1)}\hat{\boldsymbol{a}}\right]}^{\text{Term 0}}+\overbrace{\sum_{j=1}^{m}C_{j}^m\left[\partial_{\xi}^{(j)}\hat{\boldsymbol{a}},\partial_{\xi}^{(m-j+1)}\hat{\boldsymbol{a}}\right]}^{\text{Term 1}}.
\end{equation}
%**************************************
The terms in the expansion of \textit{Term 1} vanish when $j=\frac{m+1}{2}$. Keeping that in mind, we note that the expansion of \textit{Term 1} can be written in two possible ways: the first possibility is when $j>\frac{m+1}{2}$, and the second option is considering $j<\frac{m+1}{2}$. In the first option, the coefficient of all the terms in the sum will be negative, whereas, for second case, the coefficients will be positive. This owes to the anti-commutative property of Lie-brackets. We consider the second case in our derivations. 

We can further simplify \textit{Term 1}. The total number of terms present in the expanded form of \textit{Term 1} is less than $m$. This is because the terms with interchanged order of derivatives in Lie-bracket can be reduced to one term. For instance: $$c_1\left[\partial_{\xi}^{x}\hat{\boldsymbol{a}},\partial_{\xi}^{y}\hat{\boldsymbol{a}}\right]+c_2\left[\partial_{\xi}^{y}\hat{\boldsymbol{a}},\partial_{\xi}^{x}\hat{\boldsymbol{a}}\right]=(c_1-c_2)\left[\partial_{\xi}^{x}\hat{\boldsymbol{a}},\partial_{\xi}^{y}\hat{\boldsymbol{a}}\right].$$ Thus, the maximum value of $j$ is restricted by the fact that $j<\frac{m+1}{2}$ and $j\in\mathbb{Z}^+-\{0\}$. Combining these two constraints yields $\text{max}(j)=j^{\text{max}}_m$ given by Eq. \eqref{lie_brackets_der4}. However, such a reduction or simplification of \textit{Term 1} changes the coefficient by which each term in the sum is weighed. The discussion presented so far may be demonstrated, for example, for $m=4$ as: 
%**************************************
%EQUATION:  
%**************************************
\begin{equation}\label{lie_brackets_der6}
\begin{aligned}
\text{Term 1}|_{m=4}&=\sum_{j=1}^{4}C_{j}^4\left[\partial_{\xi}^{(j)}\hat{\boldsymbol{a}},\partial_{\xi}^{(5-j)}\hat{\boldsymbol{a}}\right]=3\left[\partial_{\xi}\hat{\boldsymbol{a}},\partial_{\xi}^4\hat{\boldsymbol{a}}\right]+2\left[\partial_{\xi}^2\hat{\boldsymbol{a}},\partial_{\xi}^3\hat{\boldsymbol{a}}\right]=-3\left[\partial_{\xi}^4\hat{\boldsymbol{a}},\partial_{\xi}\hat{\boldsymbol{a}}\right]-2\left[\partial_{\xi}^3\hat{\boldsymbol{a}},\partial_{\xi}^2\hat{\boldsymbol{a}}\right]\\
&=\|C_{1}^4-C_{4}^4\|\left[\partial_{\xi}\hat{\boldsymbol{a}},\partial_{\xi}^4\hat{\boldsymbol{a}}\right]+\|C_{2}^4-C_{3}^4\|\left[\partial_{\xi}^2\hat{\boldsymbol{a}},\partial_{\xi}^3\hat{\boldsymbol{a}}\right]=\sum_{j=1}^{j_{4}^{\text{max}}=2}(C_{j}^4-C_{(4-j+1)}^4)\left[\partial_{\xi}^{(j)}\hat{\boldsymbol{a}},\partial_{\xi}^{(5-j)}\hat{\boldsymbol{a}}\right].
\end{aligned}
\end{equation}
%************************************** 
For a general case, if $j_{m}^{\text{max}}\geq 1$, \textit{Term 1} can be written as:
%**************************************
%EQUATION:  
%**************************************
\begin{equation}\label{lie_brackets_der7}
\text{Term 1}=\sum_{j=1}^{j_{m}^{\text{max}}} b_{mj}\left[\partial_{\xi}^{(j)}\hat{\boldsymbol a},\partial_{\xi}^{(m-j+1)}\hat{\boldsymbol a}\right].
\end{equation}
The modified coefficient $b_{mj}=(C_{j}^m-C_{(m-j+1)}^m)$ is defined in Eq. \eqref{lie_brackets_der4}. From the second equality in Eq. \eqref{lie_brackets_der4}, we also note that $b_{m0}=C^m_0=1$. Therefore, combining \textit{Term 0} and \textit{Term 1} proves the proposition. $\square$
%######################################################################
%**************************************  
\subsection{Derivatives of curvature tensor}
The derivative of the curvature tensor may be obtained using Eq. \eqref{curv_tensor_uptheta} deploying a straightforward application of the chain rule. However, deriving the expression of higher-order derivatives using Eq. \eqref{curv_tensor_uptheta} is cumbersome because of the involvement of trigonometric functions. Instead, we realize that the reparametrization of the rotation tensor by the \textit{Gibbs vector} (the components of which are called as \textit{Gibbs} or \textit{Rodriguez parameters} in the literature) yields the formula of curvature tensor that is beneficial in obtaining the derivative of curvature tensor in the form of a single summation-formula. Consider a rotation tensor $\boldsymbol{Q}(\boldsymbol\uptheta)=\text{exp}(\hat{\boldsymbol\uptheta})\in SO(3)$. We define the Gibbs vector $\boldsymbol{\phi}$ and the associated quantities as:
%**************************************
%EQUATION:  
%**************************************
\begin{equation}\label{l34}
\begin{gathered}
\hat{\boldsymbol{\phi}}=\frac{\tan\left(\frac{\uptheta}{2}\right)}{\uptheta}\hat{\boldsymbol\uptheta};\quad \boldsymbol{\phi}=\frac{\tan\left(\frac{\uptheta}{2}\right)}{\uptheta}{\boldsymbol\uptheta};\quad \phi=\|\boldsymbol{\phi}\|=\tan\left(\frac{\uptheta}{2}\right);\quad \overline\phi=2\cos^2\left(\frac{\uptheta}{2}\right)=\frac{2}{\phi^2+1}.
\end{gathered}
\end{equation}
%**************************************
The result defined in Eq. \ref{exp_map1} may be manipulated using the definition given above as:
%**************************************
%EQUATION:  
%**************************************
\begin{equation}\label{l35}
\boldsymbol{Q}(\boldsymbol{\phi})=\boldsymbol{I}_3+\overline\phi(\hat{\boldsymbol{\phi}}+\hat{\boldsymbol{\phi}}^2).
\end{equation}
%**************************************
With this reparametrized form of rotation tensor, the curvature tensor can then be obtained in a much simpler form as:

%**************************************
%EQUATION:  
%**************************************
\begin{equation}\label{l36}
\hat{\boldsymbol{\kappa}}=\partial_{\xi}\boldsymbol{Q}.\boldsymbol{Q}^T=\overline{\phi}\left(\partial_{\xi}\hat{\boldsymbol{\phi}}+[\hat{\boldsymbol{\phi}},\partial_{\xi}\hat{\boldsymbol{\phi}}]\right).
\end{equation}
%**************************************
%######################################################################
%######################################################################
\paragraph{Proposition 6:}The $n^{\text{th}}$ order derivative of curvature tensor is given by:
%**************************************
%EQUATION:  
%**************************************
\begin{equation}\label{l38}
\partial_{\xi}^n\hat{\boldsymbol\kappa}=\sum_{i=0}^{n}C_{i}^n\partial_{\xi}^{(i)}\overline{\phi}\left(\partial_{\xi}^{(n-i+1)}\hat{\boldsymbol{\phi}}+\sum_{j=0}^{j^{\text{max}}_{(n-i)}}b_{(n-i)j}\left[\partial_{\xi}^{(j)}\hat{\boldsymbol{\phi}},\partial_{\xi}^{(n-i+1-j)}\hat{\boldsymbol{\phi}}\right]\right)
\end{equation}
%**************************************
where, $n,i,j,j^{\text{max}}_{(n-i)},C_{i}^n,b_{(n-i)j}\in\mathbb{Z}^+$. Replacing $m\longrightarrow (n-i)$ in \eqref{lie_brackets_der4} yields $j^{\text{max}}_{(n-i)}$, and $b_{(n-i)j}$.
\paragraph{Proof:}We utilize Eq. \eqref{general_der} and the expression of curvature tensor in Eq. \eqref{l36} and obtain
%**************************************
%EQUATION:  
%**************************************
\begin{equation}\label{l39a}
\partial_{\xi}^n\hat{\boldsymbol\kappa}=\sum_{i=0}^n C_{i}^n\partial_{\xi}^{(i)}\overline{\phi}.\left(\partial_{\xi}^{(n-i)}\left(\partial_{\xi}\hat{\boldsymbol{\phi}}+\left[\hat{\boldsymbol{\phi}},\partial_{\xi}\hat{\boldsymbol{\phi}}\right]\right)\right)=\sum_{i=0}^n C_{i}^n\partial_{\xi}^{(i)}\overline{\phi}.\left(\partial_{\xi}^{(n-i+1)}\hat{\boldsymbol{\phi}}+\partial_{\xi}^{(n-i)}\left[\hat{\boldsymbol{\phi}},\partial_{\xi}\hat{\boldsymbol{\phi}}\right]\right)
\end{equation}
%**************************************
Using \textit{Proposition 5} and replacing $m\longrightarrow (n-i)$, we obtain the expression of $\partial^{(n-i)}_{\xi}\left[\hat{\boldsymbol{\phi}},\partial_{\xi}\hat{\boldsymbol{\phi}}\right]$, which when substituted in \eqref{l39a} arrives at the intended result. $\square$
%######################################################################
%######################################################################
\paragraph{Corollary 1:} The following holds:
%**************************************
%EQUATION:  
%**************************************
\begin{subequations}\label{c2_1}
	\begin{align}
	&\begin{aligned}
	\tilde\partial_{\xi}^n\hat{\boldsymbol{\kappa}}&=\partial_{\xi}^n\hat{\boldsymbol{\kappa}}-(1-\delta_{n0})\sum_{i=1}^{n-1}\partial_{\xi}^{(i-1)}\left[\hat{\boldsymbol{\kappa}},\tilde\partial_{\xi}^{(n-i)}\hat{\boldsymbol \kappa}\right]=\partial_{\xi}^n\hat{\boldsymbol{\kappa}}-(1-\delta_{n0})\sum_{i=1}^{n-1}\sum_{j=0}^{i-1}C_j^{(i-1)}\left[\partial_{\xi}^{(j)}\hat{\boldsymbol{\kappa}},\partial_{\xi}^{(n-i-j)}\tilde\partial_{\xi}^{(n-i)}\hat{\boldsymbol \kappa}\right];
	\end{aligned}\label{c2_1a}\\
	&\partial_{\xi}^n\hat{\overline{\boldsymbol{\kappa}}}=\boldsymbol{Q}^T.\tilde\partial_{\xi}^n\hat{\boldsymbol{\kappa}}.\boldsymbol{Q}\label{pc_1c}
	\end{align}
\end{subequations}
%**************************************
\paragraph{Proof:} This corollary follows from Propositions 2, 3 and 4. We also note that in the sums presented above, $\text{max}(i)=(n-1)$, because $\left[\hat{\boldsymbol{\kappa}},\tilde\partial_{\xi}^{(n-i)}\hat{\boldsymbol{\kappa}}\right]\bigg|_{(n=i)}=\boldsymbol{0}_3$. $\square$ 
%######################################################################
%######################################################################
