\section{Fatigue Data Experiment}
\label{section4}
To further evaluate the effectiveness of the Bayesian model, we performed failure prediction on unaltered data from a fatigue experiment, again using inverse crack growth as the selected input feature. The fatigue experiment used in this section was first shown in the public domain in \cite{Leung2019}. The feature data was taken from a fatigue experiment using a standard 316 stainless steel compact tension test specimen, detailed in Figure (\ref{figure:f8}) and Tables (\ref{tab:t3}) and (\ref{tab:t4}). The feature was monitored using a permanently-installed potential drop measurement system, and the results plotting crack length as a function of fatigue cycle are shown in figure (\ref{figure:f8}) (right), which also shows the inverse crack growth rate. The measurement in this experiment contains observations throughout the entire crack propagation event, potentially including crack growth stages 1-3, which is shown to correspond to a transient, unkown $\alpha$ value (will be noted as $\alpha_u$)\cite{Corcoran2017,Leung2019}.

The non-stationary nature of $\alpha_u$ (nonlinearity measure) in the real, unaltered fatigue test caused an influential difference compared to the synthesized data which was generated with a constant $\bar{\alpha}$ value. Performing Bayesian inference on the entire series of data (which may include multiple stages of crack growth) can introduce biased estimation with non-stationary parameters. To mitigate the effects of this potential bias, we designed a method to truncate data estimated to be measured during different-than-current crack growth stages. We arbitrarily chose our "current time" prediction cycle to be approximately $t=0.75\bar{t_f}$ and utilized PYMC3 to develop a Bayesian model that incorporates two distinct, uncorrelated sets of $\Theta$ (all of the sampled parameters) determined by a switch point selected by the maximum posterior belief of a sampled $\alpha$ switch point parameter (noted as $\alpha_{sp}$), i.e. completely separating the sampled parameters after a discrete cycle where $\alpha$ has changed, signifying the change in crack growth stage. Though the literature shows that crack stage growth can assumed to have 3 stages, equating to two $\alpha$ switch points, we found that including two switch points created convergence issues at $t=0.75$ because the data may not have enough data points in the stage 3 regime of crack growth, governed by the third $\alpha$ parameter. We refer to this new model as the $\alpha$ switch point Bayesian model, and all other components of the model stayed consistent with the model detailed in Section (\ref{bayesian model}). 

Due to the non-correlated nature of the estimated parameters before and after the $\alpha$ switch point, sampling using the single $\alpha$ Bayesian model on data after the $\alpha$ switch point cycle provided an identical method of $t_f$ sampling as using the $\alpha$ switch point Bayesian model, but with a much lower dimensional (higher sample rate). The results presented in Figures (\ref{figure:fatdat_pre},\ref{figure:fatdat_pdf}, and\ref{figure:fatdatjoint_grid}) are produced by running the single $\alpha$ Bayesian model on data with early feature data with and without early stage data truncation.
\begin{figure}
	\includegraphics[width=1.77in]{coupon.png}
	\includegraphics[width=2.17in]{coupon2.png}
	\includegraphics[width=2.87in]{fatdat.png}
	\caption{\label{figure:f8} (left and center) Geometry of the specimen used in the fatigue experiment. (right) Feature (crack growth rate) and its inverse with respect to cycle.}
\end{figure}

\begin{table}[h]
\centering
	\caption{Fatigue test specimen dimensions}
	\label{tab:t3}
	\begin{tabular}{|l|c|} 
		\hline
		\textbf{Parameter} & \textbf{Value} \\
		
		\hline
		$W$ (mm) & 50 \\  \hline
		$B$ (mm) & 25 \\  \hline
		$a$ (mm) & 15.5 \\  \hline
		Maximum Load, $P_{max}$ (kN) & 11 \\ \hline
		Load Ratio, ${R}$ (-) & 0.1 \\ \hline
	\end{tabular}
\end{table}

\begin{table}
	\centering
	\caption{Quantified uncertainties of each input parameter of the empirical crack growth law.}
	\label{tab:t4}
	\begin{tabular}{|l|c|r|} 
		\hline
		\textbf{Parameter} &	\textbf{Mean Value} & \textbf{Standard Error} \\ \hline
		Measured Crack Length, a0 (mm) & Updates with each inspection &	1 \\\hline
		Critical Crack Length, af (mm) & 38 &Not considered \\\hline
		Paris Constant, ln(C) & -25.5 & 0.264 \\\hline
		Paris Exponent, m & 2.88 & Not considered \\\hline
		Maximum Load, Pmax (kN) & 35 & 3.5 \\\hline
		Load Ratio, R & 0.1	& Not considered \\\hline
		Geometry, Y(a) & Calculated from standards &	Not considered \\\hline
	\end{tabular}
\end{table}


Figure (\ref{figure:ramin}) shows the experimental data, Bayesian posterior prediction of experimental data, and $\hat{\alpha_{sp}}$ estimation distribution. The mean value of the estimated $\hat{\alpha_{sp}}$ was found to be cycle 70,954, with the closest data point recorded on cycle 70,776, approximately 17\% through the data collection. For further analysis, the truncation point was therefor selected as cycle 70,776.

The single $\alpha$ Bayesian model was run on data with and without truncation of data prior to the estimated switching cycle. Figure (\ref{figure:fatdat_pre}) shows an example of the posterior prediction of future data using the entirety of data at cycle 0.75$\bar{t_f}$ (cycle 0 to cycle 311,113) (left) compared to the posterior prediction of future data only using data after the $\alpha$ switch point at cycle 0.75$\bar{t_f}$ (cycle 70,776 to cycle 311,113) (right). The posterior prediction is considerably tighter when the data is truncated, despite sampling with less data, as seen comparing $t_f$ distributions in Figure (\ref{figure:fatdat_pre}) (right) and Figure (\ref{figure:fatdat_pre}) (left). Truncating the feature data to maintain an estimated more constant $\alpha$ generated a conservative $t_f$ estimate, while including data prior the $\alpha$ switch point created a much broader distribution. This qaulitative analysis is consitent with the results from the study using sysnthetic data, where data realizations with $\bar{\alpha} < 2$ caused 

which still incorporates $\bar{t_f}$ in a high probability regions, but the maximum likelihood value of the distribution overestimates $\bar{t_f}$.
\begin{figure}
	\centering
	\includegraphics[width=6.4in]{Ramin.png}
	\caption{\label{figure:ramin} $t_f$ estimation using  Bayesian switch-point model, using entirety of data until 0.75$\bar{t_f}$}
\end{figure}

\begin{figure}
	
	\includegraphics[width=3.4in]{nochoppre65.png}
	\includegraphics[width=3.4in]{choppre65.png}
	
	\caption{\label{figure:fatdat_pre} (left) Posterior prediction using entirety of cycle data, from cycle 0 to to 0.65$\bar{t_f}$. (right) Posterior prediction using cycle data, truncated at $\hat{\alpha_{sp}}$, data showing from 0.17$\bar{t_f}$ to 0.65$\bar{t_f}$}
\end{figure}
\begin{figure}
	\includegraphics[width=3.4in]{pdfcompNOCHOP.png}
	\includegraphics[width=3.4in]{pdfcompCHOP.png} 
	
	\caption{\label{figure:fatdat_pdf} (left) Linear regression and Bayesian model $t_f$ estimation using the entire data set at prescribed intervals. (right) Linear regression and Bayesian model $t_f$ estimation, data truncated at $\alpha$ switchpoint}
\end{figure}

\begin{figure}
	\includegraphics[width=2.27in]{0 no chop fatdat joint.png}
	\includegraphics[width=2.27in]{1 no chop fatdat joint.png}
	\includegraphics[width=2.27in]{2 no chop fatdat joint.png} \\
	\includegraphics[width=2.27in]{0 chop fatdat joint.png}
	\includegraphics[width=2.27in]{1 chop fatdat joint.png}
	\includegraphics[width=2.27in]{2 chop fatdat joint.png} \\
	\caption{\label{figure:fatdatjoint_grid} Joint Distributions of $t_f$ and $\alpha$ made by the Bayesian model for the un-altered fatigue experiment. The yellow $\times$ is the median prediction of the linear regression model, the blue shaded regions correspond to the Bayesian model’s joint traces, and blue dashed line is the recorded failure cycle, $\bar{t_f}$. The linear regression marker is not estimating the value of $\alpha$, and is set at 2.0 because of the definition of the linear regression.}
\end{figure}

Figure (\ref{figure:fatdat_pdf}) shows the evolution of Bayesian $t_f$ estimation and $p(\hat{t_f})$ from the linear regression model at prediction cycle  $0.35t_f$, $0.53t_f$, and $0.71t_f$, for non-truncated feature data and truncated feature data. Truncating the data from the estimated stage 1 crack growth region greatly affected the accuracy and stability of $t_f$ and $p(\hat{t_f})$ for each prediction cycle.

Figure (\ref{figure:fatdat_pdf} left) shows $p(\hat{t_f})$ from the linear regression model and the Bayesian model using the real fatigue experimental data without truncation. The linear regression $t_f$ estimations incorporated the true $t_f$ value at every prediction cycle simulated, but the maximum likelihood of the distributions were consistently negatively biased. This negative bias is consistent with the results from the simulations on synthetic data in Figure (\ref{figure:pdf_grid} top row), where $\bar{\alpha}<2$. The Bayesian model operating on the non-truncated data were much tighter, and outperformed the linear regression model (predictions at $0.35t_f$ and $0.53t_f$), where the true $\bar{t_f}$ was incorporated into high probability regions of the distribution. However, the Bayesian model performed similar to the trend of estimation in the synthetic data experiment: first underestimating $t_f$ (distribution created at $0.35\bar{t_f}$) and then overestimating $t_f$ ($0.53\bar{t_f}$ and $0.53\bar{t_f}$).

Figure (\ref{figure:fatdat_pdf} right) shows $t_f$ estimation made by both models on data which was truncated to remove the estimated stage 1 crack region. Both models produced more accurate $t_f$ estimations, except the linear regression model at cycle $0.35\bar{t_f}$, which we can assume the accuracy is impacted by the small amount of data available at that cycle after truncation (only data from $0.17t_f - 0.35t_f$ is available). The $t_f$ estimations made by the Bayesian model for all cycles performed accurately, incorporating $\bar{t_f}$ in high probability regions of estimation, even for the $0.35t_f$ prediction. These results suggest that $t_f$ estimation in both models can be improved by windowing data to include only data expected to be governed by constant empirical parameters.

Figure (\ref{figure:fatdatjoint_grid}) shows the joint $t_f$-$\alpha$ estimated parameter distributions generated by the Bayesian model on data from a real un-altered fatigue experiment first published in \cite{Leung2019}. We observed a very strong negative correlation between $t_f$ and $\alpha$ which increases in magnitude as more data becomes available to the Bayesian model (ranging from $\rho \approx -0.71$ to $-0.96$). This negative correlation observed was consistent with the the negative correlation that we observed in the simulations using synthetic data provided in Figure (\ref{figure:joint_grid}). The strong negative correlation between these two parameters motivates another iteration of the Bayesian model witch established joint distributions of the $t_f$ and $\alpha$ parameters or possible combining these terms through principal component analysis.

 

