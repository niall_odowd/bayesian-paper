\section{Predictive Models}
\label{section2} 
The most common FFM model form proposes that the time rate-of-change $R$ of some time-dependent feature $\Omega$, $R=\dot{\Omega}$, obeys the following evolution equation 
\begin{equation}
\label{eq:1}
\dot{R}=kR^\alpha
\end{equation}
where $k>0 $ and $\alpha >1$ are empirical constants relevant to the specific physical process. The solution to Eq. (\ref{eq:1}), assuming that the rate $R$ at the time of failure $\bar{t_f}$ is $R_f$ (often assumed infinite in the original literature, since the model form admits an infinite asymptote at $\bar{t_f}$) is given by
\begin{equation}\label{eq:2}
R(t)=[R^{1-\alpha}_f+k(\alpha-1)(\bar{t_f}-t)]^{\frac{1}{1-\alpha}}.
\end{equation}
It has been shown in the FFM literature that is more convenient to consider the {\em inverse} of the rate $R$, since this facilitates easier definition of the failure criterion, i.e., the inverse rate tends to zero rather than the rate tends to infinity at the time of failure. Defining the inverse rate $P=R^{-1}$, the solution Eq. (\ref{eq:2}) becomes
\begin{equation}\label{eq:3}
P(t)=[P^{\alpha-1}_f+k(\alpha-1)(\bar{t_f}-t)]^{\frac{1}{\alpha-1}},
\end{equation}
where $\bar{t_f}$ may be solved for as
\begin{equation}\label{eq:4}
\bar{t_f}=t+\frac{P^{\alpha-1}-P^{\alpha-1}_f}{k(\alpha-1)}.
\end{equation}
This form of the FFM predicts that failure will occur at some inverse feature rate-informed amount of time after present time $t$, encapsulated in the second term on the right-hand side of Equation~\ref{eq:4}.
\subsection{Linear Regression Approach}\label{LR}
The "classic" FFM approach is to assume that $\alpha$ is approximately 2 (based on a number of empirical studies across a number of failure mechanisms, as evidenced in the literature already cited), which permits a linear regression on Eq. (\ref{eq:4}), after rearrangement:
\begin{equation}\label{eq:5}
P-P_f=k\bar{t_f}-kt
\end{equation}
Clearly, as $t\rightarrow \bar{t_f}$, then $P{\rightarrow}P_f$, where the mathematically idealized target failure criterion is that $P_f=0$, corresponding to $R_f \rightarrow \infty$, as alluded to above. By setting it to any positive value, a degree of conservatism is introduced into the approach. Of course, in most practical applications, ``failure'' occurs at a point prior to an infinite data rate-of-change observation, but to be consistent with general implementation in the literature and for the purposes of parametric studies in this paper, we will employ $P_f=0$ as the failure criterion, which won't change the basic nature of this study. With that, the regression coefficients obtained from a time/data linear regression (over some given window of time) are given by $\beta_0=k\bar{t_f}$ (intercept) and $\beta_1=-k$ (slope) such that the regression-estimated time to failure is $\hat{t}_f=-\beta_0/\beta_1$, the negative of the ratio of the intercept to the slope. If $k$ is not known, a maximum likelihood estimation technique could be used, or its obtained through MCMC sampling methods. \cite{Corcoran2017,Bell2013}

We will review the uncertainty model recently developed for this linear regression process presented in detail in \cite{Todd2018}. Any given regression on some time-stamped feature set represents a "single block" observation, which is presumed representative of an ensemble population of regressions over the same time frame. Thus, the regression coefficient vector $\boldsymbol{\beta}=[\hat{\beta}_0,\hat{\beta}_1]^T$ is an estimate from populations of regression coefficients. A visualization of this process in seen in figure (\ref{figure:fa}). 

\begin{figure}[h]
	\centering
	\includegraphics[width=3.4in]{fig3.png}
	\caption{\label{figure:fa} An example of a regression block occurring at 300 cycles. The vertical solid line represents the failure point.}
\end{figure}

For a given linear regression model $ \boldsymbol{P}=\boldsymbol{T\beta+e} $, where $\boldsymbol{P}$ are observations of data, $ \boldsymbol{T}$ is the design matrix, and $\boldsymbol{e}$ are the residual differences from the linear model. it is assumed that the regression process yields residuals that are unbiased, uncorrelated normal $ \boldsymbol{e} \equiv N(\boldsymbol{0},\sigma^2\boldsymbol{I})$  under typical central limit theorem assumptions (regardless of the distribution in the regressed data, $\boldsymbol{P})$. Thus, it is known that the regression coefficients themselves have jointly normal distributions $\hat{\beta}_j \equiv N(\beta_j,\sigma^2(\boldsymbol{T}^T\boldsymbol{T})^{-1})$, $j=0,1$. An unbiased estimate of the population error variance is $\sigma^2=\|\boldsymbol{P-T\hat{\beta}}\|^2/(n-2)$ where $n$ is the number of data points used in the regression design, reduced by two since two regression coefficients were estimated in the process. Given the uncertainty models in the regression coefficients, the time-of-failure is predicted by $\hat{t}_f=-\hat{\beta}_0/\hat{\beta}_1$, the ratio of our two regression coefficients. The distribution of this ratio of correlated normal variables may be computed as
\begin{equation}\label{eq:6}
p(\hat{t}_f)=\int_{-\infty}^{\infty} \frac{p(-\hat{t}_f\hat{\beta}_1,\hat{\beta}_1)}{|-1/\hat{\beta}_1|}d\hat{\beta}_1
\end{equation}
where $p(\hat{\beta}_0,\hat{\beta}_1)$ is the bivariate normal density function. The computation of Eq. (\ref{eq:7}) yields the result


	
\begin{equation} \label{eq:7}
\begin{split}
p(\hat{t}_f)=\frac{\sqrt{1-\rho^2}\sigma_0\sigma_1e^{\frac{-\mu^2_1\sigma^2_0+2\rho\mu_0\mu_1\sigma_0\sigma_1-\mu^2_0\sigma^2_1}{2\sigma^2_0\sigma^2_1(1-\rho^2)}}}{\pi(\sigma^2_1+2\rho\sigma_0\sigma_1\hat{t}_f+\sigma^2_0\hat{t}_f^2)}+ \\ & \frac{e^-\frac{(\mu_1+\mu_0\hat{t}_f)^2}
	{2(\sigma^2_1+2\rho\sigma_0\sigma_1\hat{t_f}+\sigma^2_0\hat{t_f^2})}*erf(\frac{\mu_1\sigma_0(\rho\sigma_1+\sigma_0\hat{t}_f)-\mu_0\sigma_1(\sigma_1+\rho\sigma_0\hat{t}_f)}{\sqrt{2-2\rho^2}\sigma_0\sigma_1\sqrt{\sigma^2_1+2\rho\sigma_0\sigma_1\hat{t}_f+\sigma_0^2\hat{t}_f^2}})}{{\sqrt{2\pi}(\sigma^2_1+2\rho\sigma_0\sigma_1\hat{t}_f+\sigma^2_0\hat{t}_f^2)^{3/2}}},
\end{split}
\end{equation}
where $erf(*)$ is the error function, $\mu_j=\hat{\beta}_j, \sigma_j=\sqrt{\|\boldsymbol{P-T\hat{\beta}}\|^2(\boldsymbol{T}^T\boldsymbol{T})^{-1}_{j+1,j+1}/(n-2)} $,\linebreak $\|\boldsymbol{P-T\hat{\beta}}\|^2(\boldsymbol{T}^T\boldsymbol{T})^{-1}_{12}/(\sigma_0\sigma_1(n-2))$ for $j=0,1$, and the superscript $T$ indicates the matrix transpose; the double subscript 1,2 refers to the row-column selection of the subscripted matrix. Theoretically, since the exact population $\mu_j$ and $\sigma_j$ are not known \textit{a priori} and must be estimated from the data as presented above, a sampling distribution for the ratio mean and standard deviation should be derived, but in the present paper, the population ratio distribution, Eq. (\ref{eq:7}) will be used. It should be noted that this PDF has no analytically-calculable order statistics, since the tails are too ``fat''\cite{Hinkley1970}. However, histograms of time-of-failure data may be compared to Eq. (\ref{eq:7}) to verify that the distribution of the data is appropriately modeled such that estimating order statistics from the data itself is reasonable. Eq. (\ref{eq:7}) will be used as a PDF model that will be compared against a fully Bayesian model PDF, developed in subsequent sections.

\subsection{Bayesian Model}\label{bayesian model}
\subsubsection{Overview}
Markov chain Monte Carlo (MCMC) are used intensively in Bayesian inference. They are sampling based methods to approximate the posterior distribution of parameter of interest. These distributions describe our beliefs about a parameter after observing the data. MCMC methods are more useful when solving a Bayesian model for obtaining posterior is not mathematically feasible. In other words MCMC let us to design more flexible and complex Bayesian models with higher numbers of parameters.
At its core, the Bayesian technique operates by continuously updating posterior beliefs (distributions) of parameters as new data becomes available. The general form of Bayes' equation is
\begin{equation}\label{eq:8}
p(\Theta|D)=\frac{P(D|\Theta)\times P(\Theta)}{P(D)},
\end{equation}
where $D$ represents the data (in this case we are using inverse crack growth rate data) and $\Theta$ represents the model parameters to be estimated (akin to the regression coefficients of the previous section). 
A Bayesian model was developed to relax the $\alpha=2$ assumption made by the linear model and estimate $\bar{t_f}$ by sampling the distributions of $t_f$. After observing new data, denoted by $D=\{d_1,...,d_n\}$, model parameter beliefs are updated, $P(\Theta|D)$, influenced by the likelihood $P(D|\Theta)$ and a prior distribution, $P(\Theta)$. This prior distribution describes the modelers degree of belief about the parameter values before observing any data (which may also be based on past experience, if such experience exists). 

In the current work, the parameter vector $\Theta$ consists of empirical parameters $k$ and $\alpha$, the time of failure, $t_f$, and the standard deviation in our likelihood function $\sigma_l$. Because $t_f$ should be positive and greater than the current time of data collection $t$, it is assumed to be uniformly distributed from current time, $t$, to $\infty$. The prior for $k$ is also assumed to be an improper (it doesn't integrate to  1) uniform distribution, from 0 to $\infty$ because it is known to be positive. We chose the prior for $\alpha$ to be uniform, from 1.5-2.5 because the literature shows that this parameter is close to 2 (as per the discussion above leading to the linear regression implementation) and fluctuates in accordance to physical properties of failure mechanisms\cite{Corcoran2017}. We chose the prior for $\sigma_l$ to be half normal, a weakly informative distribution as suggested by Gelman et al.\cite{Gelman2017,Gelman2013}. Table (\ref{tab:t2}) shows a summary of the priors selected for the parameters.


\begin{table}[h]
	\centering
	\caption{Parameters of the Bayesian model}
	\label{tab:t2}
	\begin{tabular}{ |l|l| } 
		\hline
		\textbf{Parameter} & \textbf{Prior}  \\
		\hline
		$t_f$ & Improper uniform distribution with lower bound of $t$\\  \hline
		$k$ & Improper uniform distribution with lower bound of 0 \\  \hline
		$\alpha$ & Uniformly distributed between 1.5 and 2.5 \\  \hline
		$\sigma_l$ & Half normal distribution, standard deviation of 1 \\ 
		\hline
	\end{tabular}
\end{table}

Building a likelihood function might be possible if a specific forward measurement model were proposed. The current work, however, seeks to maintain a Bayesian model that is more agnostic to the specific kind of data/features $D$ are used, so a normal model for likelihood $N(\mu_{l},\sigma_{l})$ was chosen. In order to capture the heteroscedastic converging effect of the noise structure observed in real fatigue experiments \cite{Leung2019,Corcoran2017}, inference is performed in the logarithmic space, i.e. $P(D'|\Theta)=N(\mu_l,\sigma_l)$ where $D'=\log(D)$. By using inference in the logarithm domain, and maintaining the sampling of the likelihood's standard deviation $\sigma_l$ in the ``non-logarithm'' domain, we were able to capture posterior predictions matching the noise structure, seen in Figure (\ref{figure:f4}). The average of our likelihood function $\mu_{l}$ takes the form of the logarithm of the Eq. (\ref{eq:7}), containing sampled parameters $\alpha$, $k$, and $t_f$, provided by

\begin{equation}\label{eq:log(p)}
\mu_{l}=\log([P^{\alpha-1}_f+k(\alpha-1)(t_f-t)]^{\frac{1}{\alpha-1}}),
\end{equation}
where $t$ is current time (or cycle), and $P_f=0$ as the failure criterion.\par
In the present case, the Bayesian model is of dimension 4, making sampling-based algorithms like Markov chain Monte Carlo (MCMC) viable to explore the posterior belief space. We used the No-U-Turn Sampler (NUTS) within the PyMC3 python package\cite{Salvatier2016} to sample the joint posterior of parameters. The NUTS sampler is an extension to the Hamiltonian Monte Carlo (HMC) algorithm which eliminates the need to manually select a desired number of steps and their size.  NUTS works by building a set of likely candidate points spanning a wide range of the target distribution and stopping when the selection begins to double back on itself. We have made this selection because NUTS retains (and in some cases improves upon) HMC's ability to generate effectively independent samples efficiently\cite{Hoffman2014}. The initialization method of the sampler was selected as the "jitter+adapt\_diag" option built into the PYMC3 package, which worked by setting the starting point of the Bayesian sampler with a identity mass matrix, adapting a diagonal based on the variance of the tuning samples, and adding a uniform "jitter" in $[-1,1]$ to the starting point of each chain, detailed in \cite{Salvatier2016}. We used 2 chains for each simulation to verify convergence.

To generate posterior predictions using observable data, seen in Figure (\ref{figure:fatdatjoint_grid}), our model utilizes predictive inference, which is derived from the general form of the Bayesian model. After observations have been recorded in $D'$ ($D'=\log(D)$) , we can predict an unknown observable, $\tilde{D'}$, using similar Bayesian logic. The distribution of $\tilde{D'}$ is called the posterior predictive distribution, and is shown Eq. (\ref{eq:posterior_predict}), where the last step follows the assumed conditional independence of $\tilde{D'}$ and $D'$ given $\Theta$. Figure (\ref{figure:fb}) shows examples of the posterior predictive distributions at two separate cycle instances.\cite{Gelman2013}

\begin{align}\label{eq:posterior_predict}
\begin{split}
p(\tilde{D'}|D') &= \int p(\tilde{D'}|\Theta,D')p(\Theta|D')d\Theta\\
&= \int p(\tilde{D'}|\Theta)p(\Theta|D')d\Theta
\end{split}
\end{align}

To mimic the nature of failure prognosis, parameter estimations were performed by using data from a time window containing all sample points until the present cycle. This is seen in figure (\ref{figure:fb}) where the Bayesian model generated data projections based on data from the first cycles to the current cycle. The posterior predictions seen in figure (\ref{figure:fb}) are made from synthesized data described in a subsequent section.





\begin{figure}[h]
	\includegraphics[width=3.4in]{newPostPreD_6.png}
	\includegraphics[width=3.4in]{newPostPreD_8.png}
	\caption{\label{figure:fb} Predictions generated by the Bayesian model at different time windows. Figures show the progression at $0.6\bar{t_f}$ (left) and $0.8\bar{t_f}$ (right). }
\end{figure}

Figure (\ref{figure:fb}) shows a progression of the predictive capabilities of both models. The data on each plot (red points) show the data provided to the Bayesian model. The posterior prediction made by the model is shown in the shaded regions. Darker shaded regions on the plot show areas of where the posterior predictive generated more samples, corresponding to areas of higher probability of future data. The convergent nature of the noise of the prediction areas in the plots is a result of the logarithm space mean likelihood $\mu_l$ sampling with a "non-logarithm" space sampling of the likelihood standard deviation $\sigma_l$.
\subsubsection{Bayesian Model Verification}	

\begin{figure*}
	\includegraphics[width=2.27in]{250.png}
	\includegraphics[width=2.27in]{500.png}
	\includegraphics[width=2.27in]{750.png}
	\caption{\label{figure:f3} Verification of the agreement of the Bayesian posterior predictions of $t_f$, the linear regression uncertainty model, and a brute-force Monte-Carlo simulation, taken at different cycles. }
\end{figure*}

We verified the Bayesian model's posterior prediction and the linear regression model's uncertainty PDF against a Monte-Carlo brute force simulation taken from simulated data at different cycles, corresponding with 0.25$\bar{t_f}$, 0.50$\bar{t_f}$, and 0.75$\bar{t_f}$. As the simplest case, for each verification in Figure (\ref{figure:f3}), we assumed that $k$, $\alpha=2$, and $\sigma=\sigma_l$ are known in the linear regression model and the Bayesian model, at a prescribed time $t$. Figure (\ref{figure:f3}) shows $p(\hat{t_f})$ provided by the linear regression uncertainty model in Eq. (\ref{eq:7}), the posterior distribution of $t_f$ obtained from the Bayesian model, and a Monte-Carlo brute force simulation using Eq. (\ref{eq:3}) with noise added to the $P_f$ term. We observed excellent agreement in the verification process, which allows us to directly compare the two models estimation capabilities.


We will next look at comparisons between the linear regression $\hat{t_f}$ and the Bayesian model posterior predictions of $t_f$ using synthesized data based a fatigue experiment and unaltered data from a real fatigue experiment.
